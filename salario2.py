import unittest
class empleado: #defineunconceptoformadoporcódigoyvariable(datosyfunciones) (comounaplantilla)
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s): #metodoconstructor (instancia/objeto)
        self.nombre = n #losdosdatosquenecesitoparaconstruir
        self.nomina= s

    def calcula_impuestos (self): #cachodecódigoqsepuedereutilizar
        return self.nomina*0.30

    def __str__(self): #pararepresentarseconformatodestring (metodosespeciales)
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.calcula_impuestos())

class testempleado(unitest.TestCase):

    def test_construir(self): #estapruebadeberiafuncionar
        el= empleado("nombre", 5000)
        self.assertEqual(el.nomina, 5000)
    
    def test_impuestos(self): 
        el= empleado("nombre", 5000)
        self.assertEqual(el.calcula_impuestos, 15000)
    
    def test_str(self):
        el= empleado("pepe", 5000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", el.__str__())

if __name__ =="__main__":
    unittest.main()
