import unittest
from empleado import Empleado

class testempleado(unitest.TestCase):

    def test_construir(self): #estapruebadeberiafuncionar
        el= Empleado("nombre", 5000)
        self.assertEqual(el.nomina, 5000)
    
    def test_impuestos(self): 
        el= Empleado("nombre", 5000)
        self.assertEqual(el.calcula_impuestos, 15000)
    
    def test_str(self):
        el= Empleado("pepe", 5000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", el.__str__())

if __name__ == "__main__":
    unittest.main()
